from __future__ import absolute_import, unicode_literals
from celery import Celery
import os
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celery_start.settings')

app = Celery('celery_start')


app.config_from_object('django.conf:settings',namespace='CELERY')

app.conf.beat_schedule = {
    'every-15-seconds':{
        'task': 'celery_start.task.send_email',
        # 'schedule': 15,
        'schedule': crontab(minute='*/15'),
        'args': ('maheshwarisanjana07@gmail.com',)


    }
}
app.autodiscover_tasks()



# @app.task(bind=True)
# def debug_task(self):
#     print("Hello from Celery ")

@app.task(bind= True)

def debug_task(self):
    print(f'Request: {0!r}' .format(self.request))

