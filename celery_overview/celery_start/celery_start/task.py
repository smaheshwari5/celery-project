from celery import shared_task

from time import sleep, time
from django.core.mail import send_mail




@shared_task
def sum(a,b):
    time.sleep(10)
    return a+b

@shared_task
def send_email(email):
    print(f'A sample message is send to (email)')


@shared_task
def sleepy(duration):
    sleep(duration)
    return None


@shared_task
def send_email_task():
    sleep(5)
    sub= 'CELERY TASK'
    msg= "Hello , now you are added in our celery project group. Here we are talking about our celery projects and our daily work.  Thanks to join our channel"
    send_mail(sub, msg , 'maheshwarisanjana007@gmail.com' ,['maheshwarisanjana07@gmail.com'])


    return None
