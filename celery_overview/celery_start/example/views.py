from django.shortcuts import render
from django.http import HttpResponse
from celery_start.task import send_email_task

# Create your views here.

def index(request):
    send_email_task.delay()
    return HttpResponse('<h1>Email has been sent !! </h1>')