from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab
# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'perodic_mail.settings')

app = Celery('perodic_mail')

app.conf.enable_utc=False

app.conf.update(timezone='Asia/Kolkata')

app.config_from_object(settings, namespace='CELERY')

app.conf.beat_schedule = {
    'Send_mail_to_Client': {
        'task': 'perodic_mail.tasks.send_email',
        'schedule': 60.0,
        'args': ('maheshwarisanjana07@gmail.com',)

    
     
    

}
}
app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
