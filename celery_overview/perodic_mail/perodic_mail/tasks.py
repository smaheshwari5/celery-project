import imp
from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
import time


@shared_task
def send_email(email):
    print('A simple message is sent to {email}')

# def send_mail_task(email):
#     print("Mail sending.......")    
#     subject = 'welcome to Celery world'    
#     message = 'Hi thank you for using celery'    
#     email_from = settings.EMAIL_HOST_USER    
#     recipient_list = ['maheshwarisanjana07@gmail.com', ]    
#     send_mail( subject, message, email_from, recipient_list )    
#     return "Mail has been sent........"