
from email import message
from .models import Profile
import datetime
from django.core.mail import send_mail
from django.conf import settings

# @app.task(name="send_notification")
def send_notification():
    try:
        time_thresold = datetime.now()- datetime.timedelta(hours=1)
        profile_objs = Profile.objects.filter(is_verified=False ,created_at_gte = time_thresold)

        for profile_obj in profile_objs:
            subject = 'Notification !! not verified '
            message = "your account is not acctivated !!!"
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [profile_obj.email]
            send_mail(subject, message, email_from, recipient_list)
            pass

    except Exception as e:
        print(e)