from __future__ import absolute_import, unicode_literals
from celery import Celery
import os

from django.conf import settings
from celery.schedules import crontab

from example.task import send_notification


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celery_beat.settings')

app = Celery('celery_beat')


app.config_from_object('django.conf:settings',namespace='CELERY')

app.autodiscover_tasks()



@app.task(bind=True)
def debug_task(self):
    print("Hello from Celery ")


@app.task
def print_hello():
    print("Hello from Function")


app.autodiscover_tasks(lambda:settings.INSTALLED_APPS)

app.conf.beat_schedule= {
    'add-every-1-hour': {
        'task' : 'send_notification',
        'schedule': crontab(minute= '*1')
    }
}




@app.task(bind= True)
def debug_task(self):
    print(f'Request: {0!r}' .format(self.request))

