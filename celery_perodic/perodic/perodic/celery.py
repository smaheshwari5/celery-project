from __future__ import absolute_import, unicode_literals
from time import timezone
from celery import Celery
from celery.schedules import crontab


import os



os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'perodic.settings')

app = Celery('perodic')
app.conf.enable_utc = False
app.conf.update(timezone= 'Asia/kolkata')


app.config_from_object('django.conf:settings',namespace='CELERY')

app.conf.beat_schedule={
    'send-mail-every-day-at-4':{
        'task': 'send_mail.tasks.send_mail_func',
        'schedule': crontab(hour=16, minute=12),
        #'args': (2,)
    }


}

app.autodiscover_tasks()



# @app.task(bind=True)
# def debug_task(self):
#     print("Hello from Celery ")

@app.task(bind= True)

def debug_task(self):
    print(f'Request: {0!r}' .format(self.request))

